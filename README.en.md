# wanliniusdk

#### Description
万里牛 PHP-SDK

#### Software Architecture

wanliniu sdk Dependency package

```
[wanliniu api file](https://www.yuque.com/docs/share/fdd674f6-fb07-4b8d-84cc-6dea8b03c070)
```


#### Installation

```
composer require guirong/wanliniu
```


#### Instructions


```
<?php

/**
 * Test, interface call
 *
 * Wanli niuinterface provides two environments: test address and production address. SDK requests production environment by default. If you need to switch to test environment, when instantiating class wanliniuclient, specify the value of $debug parameter to true or specify by setenv() function.
 *
 * 》Example of specified environment
 * $client = new WanLiNiuClient('your app_key', 'your secret',false);
 * or
 * $client = new WanLiNiuClient('your app_key', 'your secret');
 * $client->setEnv(true);
 */

use Wanliniu\Request\Order\GetShopListRequest;
use Wanliniu\Request\Order\GetTradesDetailRequest;
use Wanliniu\Request\Order\GetTradesListRequest;
use Wanliniu\Request\Order\TradesSendRequest;
use Wanliniu\WanLiNiuClient;

/**
 * 1、Interface for querying shop order information
 */
$request = new GetShopListRequest();
$request->setSubscribeType(2);
$client = new WanLiNiuClient('your app_key', 'your secret',false);
//$client->setEnv(false);
$res = $client->send($request);
if ($res->isSuccess()) {
    print_r($res->getResult());
} else {
    echo $res->getMessage();
}


/**
 * 2、Batch order query interface
 */
$request = new GetTradesListRequest();
$request->setShopNick('your shopNick');
$request->setSubscribeType(2);
$request->setStart(date('Y-m-d H:i:s', strtotime('-365 day')));
$request->setEnd(date('Y-m-d H:i:s', strtotime('-1 day')));
$request->setPage(1);
$request->setSize(100);
$request->setStatus(GetTradesListRequest::TRADE_STATUS['complete']);
$client = new WanLiNiuClient('your app_key', 'your secret');
$res = $client->send($request);
if ($res->isSuccess()) {
    print_r($res->getResult());
} else {
    echo $res->getMessage();
}

/**
 * 3、Specify the interface for querying single order
 */
$request = new GetTradesDetailRequest();
$request->setShopNick('your shopNick');
$request->setSubscribeType(2);
$request->setTradeID('your tradeID');
$client = new WanLiNiuClient('your app_key', 'your secret');
$res = $client->send($request);
if ($res->isSuccess()) {
    print_r($res->getResult());
} else {
    echo $res->getMessage();
}

/**
 * 4、Delivery interface
 */
$request = new TradesSendRequest();
$request->setShopNick('your shopNick');
$request->setSubscribeType(2);
$request->setTradeID('your tradeID');
$request->setType(TradesSendRequest::DEVELITY_TYPE['fictitious']);
$client = new WanLiNiuClient('your app_key', 'your secret');
$res = $client->send($request);
if ($res->isSuccess()) {
    print_r($res->getResult());
} else {
    echo $res->getMessage();
}

?>
```


#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
