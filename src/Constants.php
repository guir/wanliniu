<?php
/**
 * Constants.php
 * @author   Gui Rong
 */

namespace Wanliniu;


class Constants
{
    /**
     * true 测试环境 false生产环境
     */
    const DEBUG = false;

    /**
     * Content-type
     */
    const CONTENT_TYPE = 'json';

    /**
     * 请求地址
     */

    const TEST_HOST = [
        'uri' => 'http://114.67.231.99/',  //测试地址uri
        'prefix' => 'open/api/',  //前缀
    ];

    const PRODUCTION_HOST = [
        'uri' => 'http://open.hupun.com/',  //生产地址uri
        'prefix' => 'api/',  //前缀
    ];
}
