<?php
/**
 * GetTradesDetailRequest.php
 * @author   Gui Rong
 */

namespace Wanliniu\Request\Order;


use Wanliniu\AbstractRequest;

class GetTradesDetailRequest extends AbstractRequest
{

    /**
     * 店铺昵称
     * @var string
     */
    protected $shopNick;

    /**
     * 订购类型
     * 默认 2
     * @var integer
     */
    protected $subscribeType = 2;

    /**
     * 线上订单号
     * @var string
     */
    protected $tradeID;

    public function __construct($params = [])
    {
        $url = '/v1/agent/common/trade/get';
        $method = 'GET';
        parent::__construct($url, $method, $params);
    }

    /**
     * @param string $value
     */
    public function setShopNick($value)
    {
        $this->shopNick = $value;
    }

    /**
     * @param integer $value
     */
    public function setSubscribeType($value)
    {
        $this->subscribeType = $value;
    }

    /**
     * @param string $value
     */
    public function setTradeID($value)
    {
        $this->tradeID = $value;
    }
}
