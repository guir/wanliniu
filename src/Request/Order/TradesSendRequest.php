<?php
/**
 * TradesSendRequest.php
 * @author   Gui Rong
 */

namespace Wanliniu\Request\Order;


use Wanliniu\AbstractRequest;

class TradesSendRequest extends AbstractRequest
{
    /**
     * 店铺昵称
     * @var string
     */
    protected $shopNick;

    /**
     * 订购类型
     * 默认 2
     * @var integer
     */
    protected $subscribeType = 2;

    /**
     * 线上订单号
     * @var string
     */
    protected $tradeID;

    /**
     * 物流公司代码
     * @var string
     */
    protected $delivery;

    /**
     * 运单号
     * @var string
     */
    protected $express;

    /**
     * 交易明细 id 集，以半 角逗号隔开
     * @var string
     */
    protected $oids;

    /**
     * 识别码， k1:v1;k2:v2 格式的字符串
     * @var string
     */
    protected $identity;

    /**
     * 发货类型
     * @var integer
     */
    protected $type;

    /**
     * 发货类型枚举， 0：虚拟发货，1：线下发货， 2：线上发货
     */
    const DEVELITY_TYPE = ['fictitious' => 0, 'offline' => 1, 'online' => 2,];

    /**
     * TradesSendRequest constructor.
     * @param array $params
     */
    public function __construct($params = [])
    {
        $url = '/v1/agent/common/trade/send';
        $method = 'POST';

        parent::__construct($url, $method, $params);
    }

    /**
     * @param string $value
     */
    public function setShopNick($value)
    {
        $this->shopNick = $value;
    }

    /**
     * @param integer $value
     */
    public function setSubscribeType($value)
    {
        $this->subscribeType = $value;
    }

    /**
     * @param string $value
     */
    public function setTradeID($value)
    {
        $this->tradeID = $value;
    }

    /**
     * @param string $value
     */
    public function setDelivery($value)
    {
        $this->delivery = $value;
    }

    /**
     * @param string $value
     */
    public function setExpress($value)
    {
        $this->express = $value;
    }

    /**
     * @param string $value
     */
    public function setOids($value)
    {
        $this->oids = $value;
    }

    /**
     * @param string $value
     */
    public function setIdentity($value)
    {
        $this->identity = $value;
    }

    /**
     * @param integer $value
     */
    public function setType($value)
    {
        $this->type = $value;
    }

}
