<?php
/**
 * GetTradesListRequest.php
 * @author   Gui Rong
 */

namespace Wanliniu\Request\Order;


use Wanliniu\AbstractRequest;

class GetTradesListRequest extends AbstractRequest
{
    /**
     * 店铺昵称
     * @var string
     */
    protected $shopNick;

    /**
     * 订购类型
     * 默认 2
     * @var integer
     */
    protected $subscribeType = 2;

    /**
     * 开始日期
     * @var \DateTime
     */
    protected $start;

    /**
     * 结束日期
     * @var \DateTime
     */
    protected $end;

    /**
     * 分页页码
     * @var integer
     */
    protected $page=1;

    /**
     * 每页长度
     * @var integer
     */
    protected $size=100;

    /**
     * 订单状态
     * @var integer
     */
    protected $status;

    /**
     * 订单状态枚举，1：等待付款；2：等待发货； 3：已完成；4：已关闭；5：等待确认；6： 已签收
     */
    const TRADE_STATUS = ['unpaid' => 1, 'undelivery' => 2, 'complete' => 3, 'close' => 4, 'unconfirm' => 5, 'signFor' => 6,];

    /**
     * GetTradesListRequest constructor.
     * @param array $params
     */
    public function __construct($params = [])
    {
        $url = '/v1/agent/common/trades/list';
        $method = 'GET';

        parent::__construct($url, $method, $params);
    }

    /**
     * @param string $value
     */
    public function setShopNick($value)
    {
        $this->shopNick = $value;
    }

    /**
     * @param string $value
     * Y-m-d H:i:s
     */
    public function setStart($value)
    {
        $this->start = $value;
    }

    /**
     * @param string $value
     * Y-m-d H:i:s
     */
    public function setEnd($value)
    {
        $this->end = $value;
    }

    /**
     * @param integer $value
     */
    public function setPage($value)
    {
        $this->page = $value;
    }

    /**
     * @param integer $value
     */
    public function setSize($value)
    {
        $this->size = $value;
    }

    /**
     * @param integer $value
     */
    public function setSubscribeType($value)
    {
        $this->subscribeType = $value;
    }

    /**
     * @param integer $value
     */
    public function setStatus($value)
    {
        $this->status = $value;
    }
}
