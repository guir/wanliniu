<?php
/**
 * GetShopListRequest.php
 * @author   Gui Rong
 */

namespace Wanliniu\Request\Order;


use Wanliniu\AbstractRequest;

class GetShopListRequest extends AbstractRequest
{
    /**
     * 订购类型
     * 默认 2
     * @var integer
     */
    protected $subscribeType = 2;

    /**
     * GetShopListRequest constructor.
     * @param array $params
     */
    public function __construct($params = [])
    {
        $url = 'v1/agent/common/shops/list';
        $method = 'GET';

        parent::__construct($url, $method, $params);
    }

    /**
     * @param int $subscribeType
     */
    public function setSubscribeType($subscribeType)
    {
        $this->subscribeType = $subscribeType;
    }
}
