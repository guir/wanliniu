<?php
/**
 * WanLiNiuClient.php
 * @author   Gui Rong
 */

namespace Wanliniu;


use Wanliniu\Utils\Common;

class WanLiNiuClient
{
    /**
     * @var array
     */
    protected $sysParams = [];

    public function __construct($appKey, $secret, $debug = Constants::DEBUG)
    {
        $this->sysParams['app_key'] = $appKey;
        $this->sysParams['secret'] = $secret;
        $this->sysParams['timestamp'] = intval(microtime(true) * 1000);
        $this->sysParams['format'] = Constants::CONTENT_TYPE;

        $this->setEnv($debug);
    }

    /**
     * 发送请求
     * @param AbstractRequest $request
     * @return Response
     * @throws \Exception
     */
    public function send(AbstractRequest $request)
    {
        $request->setSysParams($this->sysParams);
        return $request->send();
    }


    /**
     * 切换测试/生产环境
     * @param bool $debug
     */
    public function setEnv(bool $debug)
    {
        $debug ? Common::chooseDebugMode() : Common::chooseProductionMode();
        return $this;
    }

}
