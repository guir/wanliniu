<?php
/**
 * Response.php
 * @author   Gui Rong
 */

namespace Wanliniu;


class Response
{
    /**
     * 解析内容
     * @var array
     */
    protected $content;

    /**
     * BaseResponse constructor.
     * @param $body
     */
    public function __construct($body)
    {
        $this->content = json_decode($body, true);
    }

    /**
     * @return bool
     */
    public function isSuccess()
    {
        if (!isset($this->content['success'])) {
            return false;
        }

        if (!$this->content['success']) {
            return false;
        }

        return true;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        if (isset($this->content['error_code'])) {
            if (!is_null($this->content['error_code'])) {
                $message = 'error_code:' . $this->content['error_code'];
                return isset($this->content['error_msg']) ? $message . ',error_msg:' . $this->content['error_msg'] : $message;
            }
        }

        return '';
    }

    public function getResult()
    {
        return $this->content;
    }
}
