<?php
/**
 * HttpClient.php
 * @author   Gui Rong
 */

namespace Wanliniu\Utils;

use GuzzleHttp\Client;

/**
 * Class HttpClient
 * @package MeituanShanGou\Utils
 */
class HttpClient
{
    /**
     * @var Client
     */
    private $client;

    public function __construct()
    {
        $this->client = new Client([
            'base_uri' =>  Common::getBaseUri(),
            'timeout' => 30,
            'verify' => false
        ]);
    }

    public function get($url, $data = [])
    {
        $res = $this->client->get(Common::getUrlPrefix().$url.'?'.http_build_query($data));

        return $res->getBody();
    }

    public function post($url, $data)
    {
        $res = $this->client->post(Common::getUrlPrefix().$url, [
            'form_params' => $data
        ]);

        return $res->getBody();
    }

    public function json($method, $url, $data)
    {
        $res = $this->client->request($method, Common::getUrlPrefix().$url, [
            'json' => $data
        ]);

        return $res->getBody();
    }
}
