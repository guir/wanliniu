<?php
/**
 * Common.php
 * @author   Gui Rong
 */

namespace Wanliniu\Utils;

use Wanliniu\Constants;

class Common
{
    /**
     * @var bool
     */
    public static $debug = Constants::DEBUG;

    public static function makeSign($params, $secret)
    {
        $str = $secret;
        ksort($params);
        foreach ($params as $key => $value) {
            if (!is_null($value))
                $str .= "{$key}{$value}";
        }
        $str .= $secret;
        return strtoupper(md5($str));
    }


    public static function chooseDebugMode()
    {
        self::$debug = true;
    }

    public static function chooseProductionMode()
    {
        self::$debug = false;
    }

    public static function getBaseUri()
    {
        return self::$debug ? Constants::TEST_HOST['uri'] : Constants::PRODUCTION_HOST['uri'];
    }

    public static function getUrlPrefix()
    {
        return self::$debug ? Constants::TEST_HOST['prefix'] : Constants::PRODUCTION_HOST['prefix'];
    }
}
