# wanliniusdk

#### 介绍
万里牛 PHP-SDK

```
[万里牛接口文档](https://www.yuque.com/docs/share/fdd674f6-fb07-4b8d-84cc-6dea8b03c070) 
```


#### 软件架构

万里牛sdk依赖包


#### 安装教程

```
composer require guirong/wanliniu
```


#### 使用说明

```
<?php

/**
 * 测试,接口调用
 *
 * 万里牛接口提供 “测试地址” 和 “生产地址” 两个环境,sdk默认请求生产环境,若需要切换到测试环境,实例化类 WanLiNiuClient 时指定$debug参数值为true,或通过 setEnv() 函数指定。
 *
 * 》指定环境示例
 * $client = new WanLiNiuClient('your app_key', 'your secret',true);
 * 或
 * $client = new WanLiNiuClient('your app_key', 'your secret');
 * $client->setEnv(true);
 */

use Wanliniu\Request\Order\GetShopListRequest;
use Wanliniu\Request\Order\GetTradesDetailRequest;
use Wanliniu\Request\Order\GetTradesListRequest;
use Wanliniu\Request\Order\TradesSendRequest;
use Wanliniu\WanLiNiuClient;

/**
 * 1、查询店铺订购信息接口
 */
$request = new GetShopListRequest();
$request->setSubscribeType(2);
$client = new WanLiNiuClient('your app_key', 'your secret',false);
//$client->setEnv(true);
$res = $client->send($request);
if ($res->isSuccess()) {
    print_r($res->getResult());
} else {
    echo $res->getMessage();
}


/**
 * 2、批量查询订单接口
 */
$request = new GetTradesListRequest();
$request->setShopNick('your shopNick');
$request->setSubscribeType(2);
$request->setStart(date('Y-m-d H:i:s', strtotime('-365 day')));
$request->setEnd(date('Y-m-d H:i:s', strtotime('-1 day')));
$request->setPage(1);
$request->setSize(100);
$request->setStatus(GetTradesListRequest::TRADE_STATUS['complete']);
$client = new WanLiNiuClient('your app_key', 'your secret');
$res = $client->send($request);
if ($res->isSuccess()) {
    print_r($res->getResult());
} else {
    echo $res->getMessage();
}

/**
 * 3、指定查询单笔订单接口
 */
$request = new GetTradesDetailRequest();
$request->setShopNick('your shopNick');
$request->setSubscribeType(2);
$request->setTradeID('your tradeID');
$client = new WanLiNiuClient('your app_key', 'your secret');
$res = $client->send($request);
if ($res->isSuccess()) {
    print_r($res->getResult());
} else {
    echo $res->getMessage();
}

/**
 * 4、发货接口
 */
$request = new TradesSendRequest();
$request->setShopNick('your shopNick');
$request->setSubscribeType(2);
$request->setTradeID('your tradeID');
$request->setType(TradesSendRequest::DEVELITY_TYPE['fictitious']);
$client = new WanLiNiuClient('your app_key', 'your secret');
$res = $client->send($request);
if ($res->isSuccess()) {
    print_r($res->getResult());
} else {
    echo $res->getMessage();
}

?>
```



#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
