<?php

/**
 * 测试,接口调用
 *
 * 万里牛接口提供 “测试地址” 和 “生产地址” 两个环境,sdk默认请求生产环境,若需要切换到测试环境,实例化类 WanLiNiuClient 时指定$debug参数值为false,或通过 setEnv() 函数指定。
 *
 * 》指定环境示例
 * $client = new WanLiNiuClient('your app_key', 'your secret',false);
 * 或
 * $client = new WanLiNiuClient('your app_key', 'your secret');
 * $client->setEnv(false);
 */

use Wanliniu\Request\Order\GetShopListRequest;
use Wanliniu\Request\Order\GetTradesDetailRequest;
use Wanliniu\Request\Order\GetTradesListRequest;
use Wanliniu\Request\Order\TradesSendRequest;
use Wanliniu\WanLiNiuClient;

/**
 * 1、查询店铺订购信息接口
 */
$request = new GetShopListRequest();
$request->setSubscribeType(2);
$client = new WanLiNiuClient('your app_key', 'your secret',false);
//$client->setEnv(false);
$res = $client->send($request);
if ($res->isSuccess()) {
    print_r($res->getResult());
} else {
    echo $res->getMessage();
}


/**
 * 2、批量查询订单接口
 */
$request = new GetTradesListRequest();
$request->setShopNick('your shopNick');
$request->setSubscribeType(2);
$request->setStart(date('Y-m-d H:i:s', strtotime('-365 day')));
$request->setEnd(date('Y-m-d H:i:s', strtotime('-1 day')));
$request->setPage(1);
$request->setSize(100);
$request->setStatus(GetTradesListRequest::TRADE_STATUS['complete']);
$client = new WanLiNiuClient('your app_key', 'your secret');
$res = $client->send($request);
if ($res->isSuccess()) {
    print_r($res->getResult());
} else {
    echo $res->getMessage();
}

/**
 * 3、指定查询单笔订单接口
 */
$request = new GetTradesDetailRequest();
$request->setShopNick('your shopNick');
$request->setSubscribeType(2);
$request->setTradeID('your tradeID');
$client = new WanLiNiuClient('your app_key', 'your secret');
$res = $client->send($request);
if ($res->isSuccess()) {
    print_r($res->getResult());
} else {
    echo $res->getMessage();
}

/**
 * 4、发货接口
 */
$request = new TradesSendRequest();
$request->setShopNick('your shopNick');
$request->setSubscribeType(2);
$request->setTradeID('your tradeID');
$request->setType(TradesSendRequest::DEVELITY_TYPE['fictitious']);
$client = new WanLiNiuClient('your app_key', 'your secret');
$res = $client->send($request);
if ($res->isSuccess()) {
    print_r($res->getResult());
} else {
    echo $res->getMessage();
}
